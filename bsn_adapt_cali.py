# Python 3.x (tested with Python 3.4.2)
from statistics import median 
import os
import sys
# Always used parameters
# parameters: [var_level_1,var_level_2, stroke_volume, tentative_stroke_volume, signal_range, to_micro_g]
EMPTY_LEVEL = 70
MOVEMENT_LEVEL = 10000
MIN_VAR_LEVEL_1 = 2000
VAR1_MULTIPLIER = 10
SIGNAL_RANGE_MULTIPLIER = 40
STROKE_VOL_MULTIPLIER = 2.6
MIN_LINES = 15
# Only used if old parameters are too low/high
TOO_HIGH_PARAMETERS_MULTIPLIER = 0.75
TOO_LOW_PARAMETERS_ADDITION = 4000
ACCEPTED_RATIO_HIGH = 0.5
ACCEPTED_RATIO_LOW = 0.75
SV_DIFF_CHANGE = 0.25
def openFile():
    # Open file
    print('Input BCG data file')
    filename = str(input("Enter file BCG data file: "))
    fid = open(filename, 'r')
    return fid
def readDataToList(source):
    dataLine = source.readline()
    dataList = []
    while dataLine:
        dataLine = dataLine.strip()
        dataList.append(dataLine)
        dataLine = source.readline()
    return dataList
def readOccupied(dataList):
    signalStrengths = []
    strokeVolumes = []
    readLines = 0
    acceptedLines = 0
    svDiff = 0
    for row in dataList:
        row = row.split(',')
        if len(row) == 10: # One row of BCG data has 10 values
            row = list(map(int,row))
        else:
            continue
        signalStrengthIsOk = row[5] >= EMPTY_LEVEL and row[5] <= MOVEMENT_LEVEL
        if signalStrengthIsOk:
            readLines += 1;
            strokeVolumeIsOk = row[3] > 0
            if strokeVolumeIsOk:
                acceptedLines += 1
                signalStrengths.append(row[5])
                strokeVolumes.append(row[3])
                if acceptedLines > 1:
                    svDiff += abs(strokeVolumes[-1] - strokeVolumes[-2])
    return signalStrengths, strokeVolumes, readLines, acceptedLines, svDiff
def calibrateOccupied(oldParameters, signalStrengths, strokeVolumes, readLines, acceptedLines, svDiff):
    calcSS = median(signalStrengths)
    calcSV = median(strokeVolumes)
    acceptance = acceptedLines / readLines
    parameters = []
    if acceptedLines < MIN_LINES:
        print('Warning: Not enough occupied data. Check signal strengths.')
        parameters = oldParameters
    elif acceptance < ACCEPTED_RATIO_HIGH:
        print('Warning: Old parameters likely too high. Decreasing parameters.')
        var_level_1 = MOVEMENT_LEVEL
        stroke_vol = round(oldParameters[3] * TOO_HIGH_PARAMETERS_MULTIPLIER)
        signal_range = round(stroke_vol / STROKE_VOL_MULTIPLIER)
        parameters = list(map(int, [var_level_1, oldParameters[1], stroke_vol, oldParameters[3], signal_range, oldParameters[5]]))
    elif acceptance < ACCEPTED_RATIO_LOW and svDiff / acceptedLines < SV_DIFF_CHANGE:
        print('Warning: Old parameters likely too low. Increasing parameters.')
        var_level_1 = MOVEMENT_LEVEL
        stroke_vol = oldParameters[3] + TOO_LOW_PARAMETERS_ADDITION;
        signal_range = round(stroke_vol / STROKE_VOL_MULTIPLIER)
        parameters = list(map(int, [var_level_1, oldParameters[1], stroke_vol, oldParameters[3], signal_range, oldParameters[5]]))
    else:
        var_level_1 = max(MIN_VAR_LEVEL_1, VAR1_MULTIPLIER * calcSS)
        signal_range = calcSV * SIGNAL_RANGE_MULTIPLIER
        stroke_vol = round(STROKE_VOL_MULTIPLIER * signal_range)
        parameters = list(map(int, [var_level_1, oldParameters[1], stroke_vol, oldParameters[3], signal_range, oldParameters[5]]))
    return parameters
def readAndCalibrate(dataList, oldParameters):
    signalStrengths, strokeVolumes, readLines, acceptedLines, svDiff = readOccupied(dataList)
    parameters = calibrateOccupied(oldParameters, signalStrengths, strokeVolumes, readLines, acceptedLines, svDiff)
    return parameters
def main():
    try:
        oldParameters = [3980,EMPTY_LEVEL,2184,3385,840,7]
        print("Adaptive calibration example.")
        source = openFile()
        dataList = readDataToList(source)
        parameters = readAndCalibrate(dataList, oldParameters)
        print(parameters)
    except:
        print("Unexpected error:", sys.exc_info()[0])
    finally:
        # os.system("pause") # Win only! Can be skipped
        print ("finished")
if __name__ == '__main__':
    main()
