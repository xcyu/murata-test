#!/bin/sh
fn=$(date +"%d")_log.txt
if [ -n "$1" ]; then
    fn=$1
fi

sed -e "s/\(ip: .*\) node: \(.*\) bcg.*\('pars': '[0-9,]*'\).*\('rssi':.*\)} comm.*$/\2||\1||\3||\4/g"  $fn |sort
