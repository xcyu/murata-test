# Python >3.5 and Flask >0.11 eequired
# THIS SOFTWARE IS PROVIDED BY MURATA "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# MURATA BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
# IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
from flask import Flask, request, Response, make_response, json, send_from_directory
from werkzeug.serving import WSGIRequestHandler
from werkzeug.datastructures import Headers
import random
import statistics
import time
import string
from lxml import etree
import subprocess
#import copy
from random import *
from bod import BedOccupancyDetection
import pickle
import os.path
HOST = '0.0.0.0'  # Use host 0.0.0.0 to allow access from all devices
PORT = 10080
# PORT = 8003
app = Flask(__name__, static_url_path='')
WSGIRequestHandler.protocol_version = "HTTP/1.1"
# dataLogFilename = 'logfiles/loggedData_' + str(round(time.time())) + '.txt'
paramsLogFilename = 'parameterSync_' + str(round(time.time())) + '.txt'
OCTHRESH = 0.3
WIN_SIZE = 5
#persist to filesystem using pickle.
persist_db_path="logfiles/database.pkl"
if os.path.exists(persist_db_path) and os.path.getsize(persist_db_path) > 0:
    with open(persist_db_path,'rb') as dbf:
        data_cache =pickle.load(dbf)
        dbf.close()
else:
    data_cache={}
    #f=open(persist_db_path,'wb')
    #f.close()


@app.route('/bed/<path:path>')
def index(path):
    return send_from_directory('.', path)


@app.route('/data/pull')
def update():
    output={}
    for key, value in data_cache.items():
        output[key]={mkey: value[mkey] for mkey in ['time', 'tLeft', 'oc1','oc2','oc3','heartRate','heartRateVariance','respirationRate','sigStrength','strokeVolume']}
    return json.jsonify(output)


# @app.route('/log/')
# def log():
    # with open(dataLogFilename) as f:
        # read_data = f.read()
    # return read_data


# /data/push/ folder, accepts only POST messages
# Receives data and logs it to a file
def lmode(sequence):
    try:
        return statistics.mode(sequence)
    except statistics.StatisticsError as e:
        return sorted(sequence)[len(sequence)//2 - 1]

#@cosine(w=0.3)
@app.route('/data/push/', methods=['POST'])
def receive_data():
    #dataLogFilename = 'loggedData_'+time.strftime("%d-%b-%Y", time.gmtime())+'.txt'
    dataRecieved = request.data.decode()
    #dataParser = re.compile(r'(<Values.*?>\r\n(.+?)\r\n</Values>)', re.DOTALL)
    #text = re.findall(dataParser, dataRecieved)
# lxml parsing
    namespaces = {'ns':'urn:wsn-openapi:sidf'}
    tree = etree.fromstring(dataRecieved)
    measurement = tree.xpath("//ns:Measurement",namespaces = namespaces)
    nodeId = tree.xpath("//ns:Node/@id",namespaces = namespaces)[0]
    timeStamp = measurement[0].xpath('@time')
    values = measurement[0].xpath('//ns:Values/text()',namespaces = namespaces)[0].strip() #list of values
    dataLogFilename = 'logfiles/loggedData_' + nodeId + '.txt'
    detectionLog = 'logfiles/occupancy_' + nodeId + '.txt'
    dataLogFid = open(dataLogFilename, 'a')
    hr_sum = 0
    rr_sum = 0
    ss_sum = 0
    sv_sum = 0
    hv_sum = 0
    oc_sum = 0
    oc3 = 0
    #rec_mem = {}
    #st_pred_current = []
    global data_cache
    #prev_st = data_cache.get(nodeId).get( 'prev_st' ) if data_cache.get(nodeId) else None
    prev_oc = data_cache.get(nodeId).get( 'prev_oc' ) if data_cache.get(nodeId) else None
    rec_mem = data_cache.get(nodeId).get( 'rec_mem' ) if data_cache.get(nodeId) else None
    #bod = data_cache.get(nodeId).get( 'bod' ) if data_cache.get(nodeId) else BedOccupancyDetection()
    bod = pickle.loads(data_cache.get(nodeId).get( 'bod' )) if data_cache.get(nodeId) else BedOccupancyDetection()
    m_string = "recieved nothing"
    batch = values.splitlines()
    n = len(batch)
    for line in batch:
        ts = int(line.split(",")[0])
        hr = int(line.split(",")[1])
        rr = int(line.split(",")[2])
        sv = int(line.split(",")[3]) # avg relative stroke volume
        hv = int(line.split(",")[4]) # //avg heart rate variance
        ss = int(line.split(",")[5])
        st = int(line.split(",")[6])
        b2b = int(line.split(",")[7])
        # remove the invalide line, when sensor couldn't detect any beat, it will reproduce the previouse result for 5 more sec, so to have more accurate reading we will remove them
        if rec_mem and [hr,rr,sv,hv,b2b] == [rec_mem.get(key) for key in ['hr','rr','sv','hv','b2b']]:
            hr,rr,sv,hv,b2b=(0,)*5
            # print ('recmem', rec_mem.get('hr'), hr)
        #update with latest readings
        rec_mem = dict(ts=ts,
                       hr=hr,
                       rr=rr,
                       sv=sv,
                       hv=hv,
                       ss=ss,
                       st=st,
                       b2b=b2b
                      )
        #if not prev_st:
        #    prev_st = [st] * (WIN_SIZE)
        #st_pred_last = lmode(prev_st)
        #prev_st = prev_st[1:] + [st]
        #st_pred_current =lmode(prev_st)
        # print ('prev_st',prev_st)
        # oc = 1 if (st != 0 and 34 < hr < 121 and 8 < rr < 26) else 0
        # oc = 1 if (st_pred_current != 0) else 0
        #oc = 1 if (0 < b2b < 3000) else 0
        bod._update_state(line.split(","))
        oc = bod.old_inbed[1]
        if not prev_oc:
            prev_oc = [oc] * (WIN_SIZE)

        oc3 = round(sum(prev_oc[1:]+[oc])/WIN_SIZE)
        prev_oc = prev_oc[1:] + [oc]
        # print ('prev_oc', prev_oc)
        # TOOD: Do status machine
        hr_sum += hr
        rr_sum += rr
        sv_sum += sv
        hv_sum += hv
        ss_sum += ss
        oc_sum += oc
        # save the line in file +nodeID.txt
        dataLogFid.write(line+'\n')

    dataLogFid.close()
    # calc statistics
    hr_avg = hr_sum // n
    rr_avg = rr_sum // n
    ss_avg = ss_sum // n
    sv_avg = sv_sum // n
    hv_avg = hv_sum // n
    oc_avg = oc_sum / n
    # m_string = "Y" if 48 < hr_avg < 100 and ss_avg > 830 else "N"
    m_string = "Y" if oc_avg > OCTHRESH else "N"
    oc1 = 1 if ss_avg > 830 else 0
    # oc2 = 1 if 48 < hr_avg < 100 and ss_avg > 830 else 0
    # oc2 = 1 if oc_avg > OCTHRESH else 0
    oc2=oc
    # oc3 = 1 if 48 < hr_avg < 100 and 5 < rr_avg < 20 and ss_avg > 830 else 0
    # subprocess.Popen(
        # [
            # 'figlet  -c "H: {}  R: {}  O: {}"'.format(ss_avg, rr_avg,
                                                      # m_string)
        # ],
        # shell=True)
    # subprocess.Popen(['figlet "{}"'.format(dataRecieved)], shell=True)
    line= ",".join([timeStamp[0], str(oc), str(ss_avg)])
    ocFid = open(detectionLog, 'a')
    ocFid.write(line+'\n')
    ocFid.close()
    print ("{} {} H: {}  R: {}  O: {} {} S: {}".format(nodeId, timeStamp, hr_avg, rr_avg,  m_string, oc3, ss_avg))
    # check occupancy, if in bed, update tLeft,
    # if rec['oc2'][0] ==1:
    if oc2 ==1:
        tLeft = timeStamp
        # rec['tLeft'] = rec['time']
    # otherwise read currently cached value
    else:
        # else set a remote timestamp so we know the sensor is online but the bed has never been occupied since it got online
        tLeft= ['1984-07-04T08:00:00+08:00'] if nodeId not in data_cache else data_cache[nodeId]['tLeft']

    rec = dict(node=nodeId, time=timeStamp,
               heartRate=[hr_avg],
               respirationRate=[rr_avg],
               sigStrength=[ss_avg],
               strokeVolume=[sv_avg],
               heartRateVariance=[hv_avg],
               #prev_st=prev_st,
               prev_oc=prev_oc,
               rec_mem=rec_mem,
               bod=pickle.dumps(bod),
               #bod=bod,
               tLeft=tLeft,
               oc1=[oc1],
               oc2=[oc2],
               oc3=[oc3])

    data_cache.update({nodeId: rec})
    with open(persist_db_path,'wb') as dbfile:
        pickle.dump(data_cache,dbfile)
        dbfile.close()
    # data.stream(dict(time=[ts], heartrate=[hr_avg], respirationrate=[rr_avg]))

    return 'done'


##def newparams():
##    # Calculate random new calibration parameters, replace with own if so preferred
##    var_level_1 = random.randrange(1000, 9999, 1)
##    var_level_2 = random.randrange(100, 800, 1)
##    stroke_vol = random.randrange(2600, 6000, 1)
##    tentative_stroke_vol = 0
##    signal_range = round(stroke_vol / 2.6)
##    to_micro_g = 7
##    return ([
##        var_level_1, var_level_2, stroke_vol, tentative_stroke_vol,
##        signal_range, to_micro_g
##    ])


# /firmware/device/<mac>/ page, accepts GET and POST
# Implements OTA update function but utilizes only parameter updates
# Logs old parameters sent by BCG Sensor Node and responses with random parameters
# Can be modified to return address to downloadable firmware file etc.


# @app.route('/firmware/device/<mac>/', methods=['GET', 'POST'])
# def show_device_mac(mac):
    # # Log old parameters sent by BCG Sensor Node
    # paramsLogFid = open(paramsLogFilename, 'a')
    # t = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
    # paramsLogFid.write(t + '\n')
    # content = request.json
    # oldParameters = content['pars']
    # paramsLogFid.write(mac + ' old Parameters: ' + oldParameters + '\n')
    # # Create new parameters
    # newparameters = newparams()
    # # Create response message
    # strResp = '{\"pars\":\"'
    # strResp += str(newparameters[0]) + ','
    # strResp += str(newparameters[1]) + ','
    # strResp += str(newparameters[2]) + ','
    # strResp += str(newparameters[3]) + ','
    # strResp += str(newparameters[4]) + ','
    # strResp += str(newparameters[5]) + '\"}'
    # # Log new parameters
    # paramsLogFid.write(mac + ' new Parameters: ' + strResp[9:-2] + '\n')
    # # Create chunked response
    # respData = '{:X}\r\n{:s}\r\n0\r\n\r\n'.format(len(strResp), strResp)
    # resp = make_response()
    # resp.mimetype = 'application/json'
    # resp.data = (respData)
    # resp.headers.set('Content-Type', 'application/json')
    # resp.headers.set('Transfer-Encoding', 'chunked')
    # # Ensure no 'Content-length' is sent with chunked!
    # resp.headers.set('Content-length', None)
    # paramsLogFid.close()
    # return (resp)

#/firmware/device/<mac>/ page, accepts GET and POST
#Implements OTA update function but utilizes only parameter updates
#Logs old parameters sent by BCG Sensor Node and responses with random parameters
#Can be modified to return address to downloadable firmware file etc.
#@app.route('/firmware/device/<mac>/', methods=['GET','POST'])
#def show_device_mac(mac):
    #Log old parameters sent by BCG Sensor Node
##    paramsLogFilename = mac+'_'+paramsLogFilename
##    paramsLogFid = open(paramsLogFilename,'a')
##    t = time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime())
##    paramsLogFid.write(t + '\n')
##    content = request.json
##    oldParameters = content['pars']
##    paramsLogFid.write(mac + ' old Parameters: ' + oldParameters + '\n')
##    #Create new parameters
##    newparameters = newparams()
##    #Create response message
##    strResp = '{\"pars\":\"'
##    strResp += str(newparameters[0]) + ','
##    strResp += str(newparameters[1]) + ','
##    strResp += str(newparameters[2]) + ','
##    strResp += str(newparameters[3]) + ','
##    strResp += str(newparameters[4]) + ','
##    strResp += str(newparameters[5]) + '\"}'
##    #Log new parameters
##    paramsLogFid.write(mac + ' new Parameters: ' + strResp[9:-2] + '\n')
##    #Create chunked response
##    respData = '{:X}\r\n{:s}\r\n0\r\n\r\n'.format(len(strResp),strResp)
##    resp = make_response()
##    resp.mimetype = 'application/json'
##    resp.data = (respData)
##    resp.headers.set('Content-Type', 'application/json')
##    resp.headers.set('Transfer-Encoding' , 'chunked')
##    resp.headers.set('Content-length',None) #Ensure no 'Content-length' is sent with chunked!
##    paramsLogFid.close()
##    return(resp)

if __name__ == "__main__":
    app.debug = False  # Set debug mode on/off, debug mode is insecure!
    app.run(HOST, PORT, threaded=True)
