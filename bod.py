# -*- coding: utf-8 -*-
"""Bed Occupancy Detection for BCG.

Add explanation of this module here.

"""
import bodModule

class BedOccupancyDetection:
    def __init__(self):
            # Configuration parameters (Config struct in C).
            self.alarm_arm_time = 15
            self.probability_lp_alpha = 0.25
            self.quality_lp_alpha = 0.5
            self.movement_lp_alpha = 1
            self.after_movement_from_mid = 25
            self.wait_time_after_movement = 4
            self.c_probability_limit_change_to_e = 10
            self.c_probability_limit_change_to_o = 70
            # self.theta = []
            # self.diffs = []
            # self.means = []
            # State variables (Data struct in C).
            self.last_row = [0,0,0,0,0,0,0,0,0,0]
            self.bcg_row = [0,0,0,0,0,0,0,0,0,0]
            self.row_is_new = [0,0,0,0,0]
            self.quality = 0
            self.old_inbed = [0,0]
            self.inbed = [0,0]
            self.alarm_time = 0
            self.armed = 0
            self.alarm = 0
            self.time_since_change = 0
            self.probability_of_occupied_bed = 0
            self.continue_after_movement = 0

    def set_config(self, alarm_arm_time,
                probability_lp_alpha,
                quality_lp_alpha,
                movement_lp_alpha,
                after_movement_from_mid,
                wait_time_after_movement,
                c_probability_limit_change_to_e,
                c_probability_limit_change_to_o):
            self.alarm_arm_time = alarm_arm_time
            self.probability_lp_alpha = probability_lp_alpha
            self.quality_lp_alpha = quality_lp_alpha
            self.movement_lp_alpha = movement_lp_alpha
            self.after_movement_from_mid = after_movement_from_mid
            self.wait_time_after_movement = wait_time_after_movement
            self.c_probability_limit_change_to_e = c_probability_limit_change_to_e
            self.c_probability_limit_change_to_o = c_probability_limit_change_to_o

    def _update_state(self, bcg_row):
            """
            Update the state variables (Data struct) using the bod module.
            """
            self.last_row, self.bcg_row, self.row_is_new, self.quality, self.old_inbed, self.inbed, self.alarm_time, self.armed, self.alarm, self.time_since_change, self.probability_of_occupied_bed, self.continue_after_movement = bodModule.bod(
                bcg_row,
                self.alarm_arm_time,
                self.probability_lp_alpha,
                self.quality_lp_alpha,
                self.movement_lp_alpha,
                self.after_movement_from_mid,
                self.wait_time_after_movement,
                self.c_probability_limit_change_to_e,
                self.c_probability_limit_change_to_o,
                self.last_row,
                self.bcg_row,
                self.row_is_new,
                self.quality,
                self.old_inbed,
                self.inbed,
                self.alarm_time,
                self.armed,
                self.alarm,
                self.time_since_change,
                self.probability_of_occupied_bed,
                self.continue_after_movement)
            # print (self.continue_after_movement)

    def run(self, bcg_row):
        """
        Do something with the state variables
        Decide what to return.
        param bcg_row: list of 10 bcg data values.
        """
        # Update state variables by running the
        # bed occupancy detection algorithm.
        self._update_state(bcg_row)
        # Do something with updated state variables.
        print(self.old_inbed)
