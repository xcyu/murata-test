from urllib import request, parse
import json
import bsn_adapt_cali
from IC import IntelligentCalibration
from multiprocessing.dummy import Pool as ThreadPool
TARGET_INT = 30
TARGET_URL = "http://10.0.48.21:8003"
# TARGET_URL = "http://astri.iohub.ml"
PARS_DATA = {
    # "pars": "3980,253,2184,3385,840,7"
    "pars": "3385, 260, 1352, 3385, 520, 7"

}
iplist = ['10.0.48.{}'.format(i) for i in range(1,255)]
# iplist = ['192.168.1.{}'.format(i) for i in range(190,193)]
def configIt(ip):
    data = {}
    # print (ip)
    comm_url ='http://{}/sys/comm'.format(ip)
    # prepare post
    data = getReq(comm_url)
    if data:
        # data['url']= TARGET_URL
        data['report_interval']= TARGET_INT
        data['reset_interval']= TARGET_INT
        data['url']= TARGET_URL
        # print ("update:",data)
        postReq(ip, comm_url, data)
        # TODO: update bcg
        filename='logfiles/loggedData_' + data['node_id'] + '.txt'
        # print (filename)
        with open(filename) as source:
            bcg_pars_url = 'http://{}/bcg/pars'.format(ip)

            # adaptive cali:
            # oldPars=[int(i) for i in getReq(bcg_pars_url)['pars'].split(',')]
            # dataList = bsn_adapt_cali.readDataToList(source)
            # dataList = dataList[-12*3600:-4*3600]
            # # parameters = bsn_adapt_cali.readAndCalibrate(dataList, oldPars)
            # parameters =[3600, 280, 1500, 1500, 600, 7]
            # print ('ip:', ip, 'node:', data['node_id'],'old pars:', oldPars,'new pars', parameters)

            # intelicali:

            dataLine = source.readline()
            dataList = []
            nRows = 0
            while dataLine:
                dataList.extend(dataLine.strip().split(','))
                dataLine = source.readline()
                nRows +=1

            ic = IntelligentCalibration()
            ic.initialize(empty_level   = 0,
              interval      = 60,
              std_start     = 0,
              std_stop      = 200,
              std_length    = 41,
              mean_start    = 0,
              mean_stop     = 1000,
              mean_length   = 51)
            parameters = ic.run(dataList, nRows)
            # if parameters == (0.0,0.0,0.0,0.0,0.0,0.0):
              # parameters =(4000, 259, 1600, 1600, 700, 7)
              # bad idea to reset the pars as there are dc frequently on sensor rather than only when change of residents
            if parameters !=(0.0,0.0,0.0,0.0,0.0,0.0):
                postReq(ip, bcg_pars_url, {'pars': ','.join(str(int(x)) for x in parameters)})
        # # mode 0: bcg 1: datalogger

        # static parameters:
        # parameters =[3600, 260, 1500, 1500, 600, 7]
        # parameters =[4000, 259, 2210, 2210, 850, 7]
        # parameters =[4000, 259, 1600, 1600, 700, 7]
        # bcg_pars_url = 'http://{}/bcg/pars'.format(ip)
        # postReq(ip, bcg_pars_url, {'pars': ','.join(str(x) for x in parameters)})

        MODE = {
            "mode": 0
        }
        bcg_mode_url = 'http://{}/bcg/mode'.format(ip)
        postReq(ip, bcg_mode_url, MODE)
        # direction 1: normal 0: inverted
        DIR = {
            "dir": 1
        }
        bcg_dir_url = 'http://{}/bcg/dir'.format(ip)
        postReq(ip, bcg_dir_url, DIR)
        # Config Ota
        ota_url = 'http://{}/sys/ota'.format(ip)
        OTA = {
            "autoupd": 1,
            "url": TARGET_URL,
            "username": "sensoruser"
        }
        postReq(ip, ota_url, OTA)
        # TODO: restart
        RESTART = {"cmd": "reboot"}
        cmd_url =  'http://{}/sys/cmd'.format(ip)
        postReq(ip, cmd_url, RESTART)

def getReq(url):
    orig = {}
    try:
        req  = request.Request(url)
        req.add_header('Authorization','Basic YWRtaW46YWRtaW4=')
        resp = request.urlopen(req,timeout=15).read().decode('utf8')
        # print (resp)
        orig=json.loads(resp)
        # print ("orig:",orig)
    except:
        pass
    return orig


def postReq(ip, url,data):
        req2= request.Request(url, json.dumps(data).encode('utf-8'), headers={'content-type': 'application/json'})
        req2.add_header('Authorization','Basic YWRtaW46YWRtaW4=')
        resp = request.urlopen(req2,timeout=15)
        print (ip,resp.read(),url,"updated")
pool = ThreadPool(100)
pool.map(configIt,iplist)
pool.close()
pool.join()
