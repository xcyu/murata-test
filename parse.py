from lxml import etree
filename = 'loggedData_1520845743.txt'
# filename = str(input("Enter file BCG data file: "))
# NODEID = str(input("Enter bed id: "))
NODEID = '203-2'
data={}


def parseXml(xml):
    # print(xml)
    namespaces = {'ns':'urn:wsn-openapi:sidf'}
    tree = etree.fromstring(''.join(xml))
    measurement = tree.xpath("//ns:Measurement",namespaces = namespaces)
    nodeId = tree.xpath("//ns:Node/@id",namespaces = namespaces)
    timeStamp = measurement[0].xpath('@time')
    value = measurement[0].xpath('//ns:Values/text()',namespaces = namespaces)[0].strip() #list of values
    if nodeId[0] == NODEID:
        print(value[0])
    if len(value.split(',')) == 10:
        data[nodeId]+=value if nodeId in data else [value]
        print (data)

singleXml = []
with open(filename) as f:
    while True:
        line = f.readline()
        if line:
            if line.startswith('<Data'):
                if singleXml:
                    parseXml(singleXml)
                    singleXml = []
                    singleXml.append(line)
                else:
                    singleXml.append(line)

            else:
                singleXml.append(line)
        else:
            parseXml(singleXml)
            break
