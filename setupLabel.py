from urllib import request, parse
import json
import bsn_adapt_cali
from multiprocessing.dummy import Pool as ThreadPool
labels= {
    "00AEFAAEC888" :   "301-1" ,
    "00AEFAAFD064" :   "301-2" ,
    "00AEFAADD2EB" :   "301-3" ,
    "00AEFAA5A99B" :   "302-1" ,
    "00AEFA384642" :   "302-2" ,
    "00AEFAA074ED" :   "302-3" ,
    "00AEFAAE073E" :   "303-1" ,
    "00AEFAA63E3F" :   "303-2" ,
    "00AEFA3BCFA6" :   "303-3" ,
    "00AEFAAF1FAC" :   "304-1" ,
    "00AEFAAFED2C" :   "304-2" ,
    "00AEFAADD38D" :   "304-3" ,
    "00AEFAADF112" :   "304-4" ,
    "00AEFA323D81" :   "305-1" ,
    "00AEFAAF49FD" :   "305-2" ,
    "00AEFA36EB83" :   "305-3" ,
    "00AEFAAE68E5" :   "306-1" ,
    "00AEFAAF92F9" :   "306-2" ,
    "00AEFAAE03F9" :   "306-3" ,
    "00AEFAAEDD04" :   "307-1" ,
    "00AEFA32EE81" :   "307-2" ,
    "00AEFAA07871" :   "308-1" ,
    "00AEFAAFC527" :   "308-2" ,
    "00AEFAADEE54" :   "308-3" ,
    "FCDBB39C32CE" :   "308-4" ,
    "00AEFAAEB08E" :   "308-5" ,
    "00AEFAAF90E9" :   "309-1" ,
    "00AEFAAE2CF7" :   "309-2" ,
    "00AEFA34FCFE" :   "309-3" ,
    "00AEFAAF6B69" :   "309-4" ,
    "00AEFA349D7F" :   "310-1" ,
    "00AEFAADF076" :   "310-2" ,
    "00AEFAA04C52" :   "310-3" ,
    "00AEFA3632C1" :   "310-4" ,
    "00AEFAAF29ED" :   "3ISO-1" 
}
TARGET_INT = 30
# TARGET_URL = "http://10.0.48.21:8003"
TARGET_URL = "http://astri.iohub.ml"
# PARS_DATA = {
    # "pars": "3980,253,2184,3385,840,7"
    # "pars": "3385, 253, 1352, 3385, 520, 7"

# }
iplist = ['10.0.48.{}'.format(i) for i in range(1,255)]
# iplist = ['192.168.1.{}'.format(i) for i in range(190,193)]
def configIt(ip):
    data = {}
    # print (ip)
    comm_url ='http://{}/sys/comm'.format(ip)
    mac_url ='http://{}/sys/mac'.format(ip)
    # prepare post
    mac = getReq(mac_url)
    if mac:
        if mac['mac'] in labels:
            label=labels[mac['mac']]
            data=getReq(comm_url)
            print ("update label for" , mac['mac'], 'from', data['node_id'], 'to', label, 'ip', ip)
            data['node_id']= label
            postReq(ip, comm_url, data)
            # TODO: restart
            RESTART = {"cmd": "reboot"}
            cmd_url =  'http://{}/sys/cmd'.format(ip)
            postReq(ip, cmd_url, RESTART)

def getReq(url):
    orig = {}
    try:
        req  = request.Request(url)
        req.add_header('Authorization','Basic YWRtaW46YWRtaW4=')
        resp = request.urlopen(req).read().decode('utf8')
        # print (resp)
        orig=json.loads(resp)
        # print ("orig:",orig)
    except:
        pass
    return orig

def postReq(ip, url,data):
        req2= request.Request(url, json.dumps(data).encode('utf-8'), headers={'content-type': 'application/json'})
        req2.add_header('Authorization','Basic YWRtaW46YWRtaW4=')
        resp = request.urlopen(req2)
        print (ip,resp.read(),url,"updated")

pool = ThreadPool(200)
pool.map(configIt,iplist)
pool.close()
pool.join()
