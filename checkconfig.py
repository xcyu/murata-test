from urllib import request, parse
import json
import bsn_adapt_cali
from multiprocessing.dummy import Pool as ThreadPool
TARGET_INT = 30
# TARGET_URL = "http://10.0.48.21:8003"
TARGET_URL = "http://astri.iohub.ml"
# PARS_DATA = {
    # "pars": "3980,253,2184,3385,840,7"
    # "pars": "3385, 253, 1352, 3385, 520, 7"

# }
iplist = ['10.0.48.{}'.format(i) for i in range(1,255)]
# iplist = ['192.168.1.{}'.format(i) for i in range(190,193)]
def configIt(ip):
    data = {}
    # print (ip)
    comm_url ='http://{}/sys/comm'.format(ip)
    # prepare post
    data = getReq(comm_url)
    if data:
        # data['url']= TARGET_URL
        # data['report_interval']= TARGET_INT
        # data['reset_interval']= TARGET_INT
        # data['url']= TARGET_URL
        # print ("update:",data)
        # postReq(ip, comm_url, data)
        # TODO: update bcg
        filename='logfiles/loggedData_' + data['node_id'] + '.txt'
        # print (filename)
        bcg_pars_url = 'http://{}/bcg'.format(ip)
        rssi_url='http://{}/sys/rssi'.format(ip)
        print ('ip: ', ip, 'node: ', data['node_id'], 'bcg: ', getReq(bcg_pars_url), 'rssi: ', getReq(rssi_url), 'comm: ', data, )
        # mode 0: bcg 1: datalogger

def getReq(url):
    orig = {}
    try:
        req  = request.Request(url)
        req.add_header('Authorization','Basic YWRtaW46YWRtaW4=')
        resp = request.urlopen(req).read().decode('utf8')
        # print (resp)
        orig=json.loads(resp)
        # print ("orig:",orig)
    except:
        pass
    return orig


pool = ThreadPool(200)
pool.map(configIt,iplist)
pool.close()
pool.join()
