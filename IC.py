# -*- coding: utf-8 -*-
"""Intelligent Calibration for BCG.

Add explanation of this module here.

"""

from intcalModule import intelligent_calibration as intcal

class IntelligentCalibration:

	def __init__(self):

		# Intelligent calibration algorithm parameters.
		self.empty_level    = 0
		self.interval       = 0
		self.std_start      = 0
		self.std_stop       = 0
		self.std_length     = 0
		self.mean_start     = 0
		self.mean_stop      = 0
		self.mean_length    = 0

		# Number of values in one BCG row.
		self.NUM_VALUES_IN_ROW = 10

	def initialize(self, empty_level, interval, std_start, std_stop, std_length, 							 mean_start, mean_stop, mean_length):
		"""Initialize the intelligent calibration algorithm. """

		self.empty_level = empty_level
		self.interval = interval
		self.std_start = std_start
		self.std_stop = std_stop
		self.std_length = std_length
		self.mean_start = mean_start
		self.mean_stop = mean_stop
		self.mean_length = mean_length

	def run(self, inputdata, nRows):
		""" Run the intelligent calibration algorithm. """

		return intcal(inputdata, nRows, 
						self.empty_level, self.interval,
						self.std_start, self.std_stop,
						self.std_length, self.mean_start,
						self.mean_stop, self.mean_length)

